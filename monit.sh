#!/bin/bash

OS=`uname -s`

if [[ "${OS}" == "SunOS" ]]; then
        echo "OS is SunOS"
        LOGPATH=/var/log/auth.log
elif [[ "${OS}" == "Linux" ]]; then
        echo "OS is Linux"
        if [[ -f /etc/debian_version ]]; then
                echo "Linux Base is Debian"
                LOGPATH=/var/log/auth.log
        elif [[ -f /etc/redhat-release ]]; then
                echo "Linux Base is Redhat"
                LOGPATH=/var/log/secure
        elif [[ -f /etc/SuSE-release ]]; then
                echo "Linux Base is SuSE"
                LOGPATH=/var/log/message
        elif [[ -f /etc/arch-release ]]; then
                echo "Linux Base is Arch"
                LOGPATH=/var/log/auth.log
        fi
fi

while true; do
        echo -e "Metrics for ssh log-in attempts\n"
        for n in $(grep "ssh2" ${LOGPATH} |awk '{print $6"-"$11}')
        do
                i=`echo $n |cut -d- -f2`
                s=`echo $n |cut -d- -f1`
                h=`dig -x $i +short`
                if [[ $h == '' ]]; then
                       h=$i
                fi
                echo "$s $h"
        done |sort -nr | uniq -c |awk {'print $3,"\thad",$1,$2,"attempts"'}
        sleep 1
        clear
done