### PART 1

Show metrics for ssh log-in attempts on AlphaServer with following command : 

```
$ sudo bash monit.sh
```
![monit](./img/monit.png)

### PART 2

Deploy EC2 Instance by using Terraform with predefined variables.

```
$ terraform init
$ terraform plan -out=plan/init-plan-v0.1
$ terraform apply plan/init-plan-v0.1
```
